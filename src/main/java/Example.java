
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
class Example {

   public static int add(int x, int y) {
        return x + y;
    }

    static String Chop(char player1, char player2) {
        if (player1 == 's' && player2 == 'p') {
            return "p1";
        } else if (player1 == 'h' && player2 == 's') {
            return "p1";
        } else if (player1 == 'p' && player2 == 'h') {
            return "p1";
        } else if (player2 == 'p' && player1 == 'h') {
            return "p2";
        } else if (player2 == 's' && player1 == 'p') {
            return "p2";
        } else if (player2 == 'h' && player1 == 's') {
            return "p2";
        }
        return "draw";
    }
    public static void main(String[] args) {
        Scanner kb = new Scanner (System.in);
        System.out.print("Please input player 1 (p,h,s): ");
        String player1 = kb.next();
        System.out.print("Please input player 2 (p,h,s): ");
        String player2 = kb.next();
        String winner = Chop(player1.charAt(0), player2.charAt(0));
        System.out.println("Winner is "+winner);
    }
}
