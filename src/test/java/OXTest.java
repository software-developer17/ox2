/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author acer
 */
public class OXTest {
    
    public OXTest() {
        
    }
    
    @Test
    public void testCheckVerticalPlayerXCol1() {
        char table[][] = {{'X', '-', '-'},
                                {'X', '-', '-'},
                                {'X', '-', '-'}};
        char player = 'X';
        int col = 1;
        assertEquals(true, ox.checkVertical(table, player, col));
    }
    @Test
    public void testCheckVerticalPlayerXCol2() {
        char table[][] = {{'-', 'X', '-'},
                                {'-', 'X', '-'},
                                {'-', 'X', '-'}};
        char player = 'X';
        int col = 2;
        assertEquals(true, ox.checkVertical(table, player, col));
    }
    @Test
    public void testCheckVerticalPlayerXCol3() {
        char table[][] = {{'-', '-', 'X'},
                                {'-', '-', 'X'},
                                {'-', '-', 'X'}};
        char player = 'X';
        int col = 3;
        assertEquals(true, ox.checkVertical(table, player, col));
    }
    @Test
    public void testCheckHorizontalPlayerXRow1() {
       char table[][] = {{'X', 'X', 'X'},
                                {'-', '-', '-'},
                                {'-', '-', '-'}};
        char player = 'X';
        int row = 1;
        assertEquals(true, ox.checkHorizontal(table, player, row));
    }
    @Test
    public void testCheckHorizontalPlayerXRow2() {
        char table[][] = {{'-', '-', '-'},
                                {'X', 'X', 'X'},
                                {'-', '-', '-'}};
        char player = 'X';
        int row = 2;
        assertEquals(true, ox.checkHorizontal(table, player, row));
    }
    @Test
    public void testCheckHorizontalPlayerXRow3() {
        char table[][] = {{'-', '-', '-'},
                                {'-', '-', '-'},
                                {'X', 'X', 'X'}};
        char player = 'X';
        int row = 3;
        assertEquals(true, ox.checkHorizontal(table, player, row));
    }
    @Test
    public void testCheckCrossPlayerX1() {
        char table[][] = {{'X', '-', '-'},
                                {'-', 'X', '-'},
                                {'-', '-', 'X'}};
        char player = 'X';
        assertEquals(true, ox.checkX(table, player));
    }
    @Test
    public void testCheckCrossPlayerX2() {
        char table[][] = {{'-', '-', 'X'},
                                {'-', 'X', '-'},
                                {'X', '-', '-'}};
        char player = 'X';
        assertEquals(true, ox.checkX(table, player));
    }
    @Test
    public void testCheckDraw() {
        char table[][] = {{'X', 'O', 'X'},
                                {'O', 'X', 'O'},
                                {'O', 'X', 'O'}};
        assertEquals(true, ox.checkDraw(table));
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
